
var AWS = require('aws-sdk');
var CfnLambda = require('cfn-lambda');

var ES = new AWS.ES({apiVersion: '2015-01-01'});
var Lambda = new AWS.Lambda({apiVersion: '2015-03-31'});

var async = require('async')
const semver = require('semver')

const DISABLE_RESOURCE_DELETION = process.env.DISABLE_RESOURCE_DELETION || false;
const ELASTICSEARCH_CREATION_VERSION = process.env.ELASTICSEARCH_CREATION_VERSION || "5.6";

var BoolProperties = [
  'EBSOptions.EBSEnabled',
  'ElasticsearchClusterConfig.DedicatedMasterEnabled',
  'ElasticsearchClusterConfig.ZoneAwarenessEnabled'
];

var NumProperties = [
  'EBSOptions.Iops',
  'EBSOptions.VolumeSize',
  'ElasticsearchClusterConfig.DedicatedMasterCount',
  'ElasticsearchClusterConfig.InstanceCount',
  'SnapshotOptions.AutomatedSnapshotStartHour'
];

function getPhysicalId(data, params) {
  return params.DomainName;
}

//
// DESCRIBE FUNCTION
// for faking use of function when we don't want to perform a write
//

var Describe = CfnLambda.SDKAlias({
  api: ES,
  method: 'describeElasticsearchDomain',
  keys: ['DomainName'],
  returnPhysicalId: getPhysicalId
});

//
// DELETE FUNCTION
//

var Delete = CfnLambda.SDKAlias({
  api: ES,
  method: 'deleteElasticsearchDomain',
  ignoreErrorCodes: [404, 409],
  keys: ['DomainName'],
  returnPhysicalId: getPhysicalId
});

//
// CREATE FUNCTION
//

var Create = CfnLambda.SDKAlias({
  api: ES,
  method: 'createElasticsearchDomain',
  forceBools: BoolProperties,
  forceNums: NumProperties,
  returnPhysicalId: getPhysicalId
});

//
// UPGRADE FUNCTION
//

var UpdateEngineVersion = CfnLambda.SDKAlias({
  api: ES,
  method: 'upgradeElasticsearchDomain',
  // takes key name passed from custom resource and maps it tokey name that SDK expects for this API operation
  mapKeys: {
    ElasticsearchVersion: 'TargetVersion'
  },
  // sends only a subset of keys to the API call from the custom resource parameters
  keys: ['DomainName', 'ElasticsearchVersion'],
  returnPhysicalId: getPhysicalId
});

//
// UPDATE FUNCTION
//

function setupUpdateFunction(properties, callback) {
  // some inexplicable error necessitates this - its in cfn-lambda/SDKAlias, too
  delete properties.ServiceToken

  // UPDATE FUNCTION
  // defined here to have access to event properties
  var UpdateDomainConfiguration = CfnLambda.SDKAlias({
    api: ES,
    method: 'updateElasticsearchDomainConfig',
    keys: properties,
    forceBools: BoolProperties,
    forceNums: NumProperties,
    returnPhysicalId: getPhysicalId
  });

  callback(null, UpdateDomainConfiguration);
}

//
// MAIN HANDLER
//

exports.handler = function(event, context) {
  let tasks = [];

  // setup initial custom resource actions
  let createFunction = Create;
  let deleteFunction = Delete;

  if (event.RequestType == 'Delete' && DISABLE_RESOURCE_DELETION == true) {
    deleteFunction = Describe;
    console.log('WARNING: The domain resource has deletion disabled. Nooping for success.');
  }

  if (event.RequestType == 'Create' && ELASTICSEARCH_CREATION_VERSION && !event.ResourceProperties.ElasticsearchVersion) {
    event.ResourceProperties.ElasticsearchVersion = ELASTICSEARCH_CREATION_VERSION;
    console.log('INFO: Creating domain resource with default ElasticSearch version ' + ELASTICSEARCH_CREATION_VERSION);
  }

  tasks.push(
    // pass domain description to define update function
    async.apply(setupUpdateFunction, event.ResourceProperties)
  );

  //
  // DETERMINE ENGINE UPGRADE
  //

  function checkDomain(update, callback) {
    // initially set to perform a domain configuration update
    let updateFunction = update;

    // skip create and delete operations
    let updateOnly = false;

    // if domain is false, the factory will handle the error condition
    const domain = event.ResourceProperties.DomainName ? event.ResourceProperties.DomainName : undefined
    const version = event.ResourceProperties.ElasticsearchVersion ? event.ResourceProperties.ElasticsearchVersion : undefined

    // console.log('resource properties version: '+event.ResourceProperties.ElasticsearchVersion);

    ES.describeElasticsearchDomain(
      { DomainName: domain },
      function(err, data) {
        // TODO: errors arent trickling down and getting printed by callback. not sure why
        // if (err) console.log(err);
        // console.log('describe domain version: '+data.DomainStatus.ElasticsearchVersion);

        // guard for if the domain is created and not processing other changes
        if (
            !err &&
            data.DomainStatus.Endpoint &&
            !data.DomainStatus.Processing &&
            !data.DomainStatus.UpgradeProcessing
        ) {
          // send upgrade operation if engine version is lower than intended
          if (
            version &&
            semver.lt(
              semver.coerce(data.DomainStatus.ElasticsearchVersion),
              semver.coerce(version)
            )
          ) {
            updateFunction = UpdateEngineVersion;
            updateOnly = true;
            console.log('INFO: Existing domain has lesser enginge version than intended. Will upgrade engine!');
          // do nothing if rolling back after successful upgrade or correcting template mismatch
          } else if (
            version &&
            semver.gt(
              semver.coerce(data.DomainStatus.ElasticsearchVersion),
              semver.coerce(version)
            )
          ) {
            updateFunction = Describe;
            console.log('INFO: Existing domain has greater enginge version than intended. Nooping for success.');
          } else {
            // remove domain version paramter if it appears we intend to update configuration
            // (if there is no version passed in to CR, or its the same as the domain version)
            delete event.ResourceProperties.ElasticsearchVersion
          }

          // when creating, but domain already exists, effectively noop
          if (event.RequestType == 'Create')  updateOnly = true;
        }

        if (updateOnly == true) {
          createFunction = Describe;
          deleteFunction = Describe;
        }

        callback(
          err ? err : null,
          updateFunction
        );
      }
    );
  }

  tasks.push(checkDomain);

  async.waterfall(tasks, function (err, update) {
    if (err)  console.log(err);

    // return event in case this all goes horribly wrong
    // console.log("DEBUG:\n"+event);

    //
    // CUSTOM RESOURCE HANDLER
    //

    const execute = CfnLambda({
      Create: createFunction,
      Update: update,
      Delete: deleteFunction,
      NoUpdate: NoUpdate,
      TriggersReplacement: ['DomainName'],
      SchemaPath: [__dirname, 'schema.json'],
      LongRunning: {
        PingInSeconds: 60,
        MaxPings: 55,
        TimeoutFails: false,
        LambdaApi: Lambda,
        Methods: {
          Create: CheckCreate,
          Update: CheckUpdate,
          Delete: CheckDelete
        }
      }
    });
    execute(event, context);
  });
}

function CheckProcessComplete(params, reply, notDone) {
  ES.describeElasticsearchDomain({
    DomainName: params.DomainName
  }, function(err, domain) {
    if (err) {
      console.error('Error when pinging for Processing Complete: %j', err);
      return reply(err.message);
    }
    if (
      domain.DomainStatus.Processing
      || domain.DomainStatus.UpgradeProcessing
      ||  !domain.DomainStatus.Endpoint
    ) {
      console.log('Status is not Processing: false yet. Ping not done: %j', domain);
      return notDone();
    }
    console.log('Status is Processing: false! %j', domain);
    // NOTE: we are using the response from describeElasticsearchDomain here instead of
    // the DomainName passed through CR params, as getPhysicalId does
    // this is what is responsible for returning the CR's physical ID on creation,
    //  used by references in CFN.
    // TODO: to make the returnPhysicalId key usage in SDKAlias be intuitive,
    // test this with Params.DomainName and change if it works
    reply(null, domain.DomainStatus.DomainName, {
      DomainArn: 'arn:aws:es:' + CfnLambda.Environment.Region + ':' + CfnLambda.Environment.AccountId + ':domain/' + domain.DomainStatus.DomainName,
      DomainEndpoint: domain.DomainStatus.Endpoint
    });
  });
}

function CheckCreate(createReponse, params, reply, notDone) {
  CheckProcessComplete(params, reply, notDone);
}

// TODO: the number of inputs seems unnecessary, here
function CheckUpdate(updateResponse, physicalId, params, oldParams, reply, notDone) {
  CheckProcessComplete(params, reply, notDone);
}

function CheckDelete(deleteResponse, physicalId, params, reply, notDone) {
  ES.describeElasticsearchDomain({
    DomainName: params.DomainName
  }, function(err, domain) {
    if (err && (err.statusCode === 404 || err.statusCode === 409)) {
      console.log('Got a 404 on delete check, implicit Delete SUCCESS: %j', err);
      return reply(null, physicalId);
    }
    if (err) {
      console.error('Error when pinging for Delete Complete: %j', err);
      return reply(err.message);
    }
    if (domain.DomainStatus.Processing) {
      console.log('Status is not Deleted yet. Ping not done: %j', domain);
      return notDone();
    }
    console.log('Status is Deleted! %j', domain);
    reply(null, domain.DomainStatus.DomainId);
  });
}

function NoUpdate(phys, params, reply) {
  ES.describeElasticsearchDomain({
    DomainName: params.DomainName
  }, function(err, domain) {
    if (err) {
      console.error('Error when pinging for NoUpdate Attrs: %j', err);
      return reply(err.message);
    }
    console.log('NoUpdate pingcheck success! %j', domain);
    reply(null, domain.DomainStatus.DomainId, {
      DomainArn: 'arn:aws:es:' + CfnLambda.Environment.Region + ':' + CfnLambda.Environment.AccountId + ':domain/' + domain.DomainStatus.DomainName,
      DomainEndpoint: domain.DomainStatus.Endpoint
    });
  });
}
